import java.io.File;
import java.io.IOException;
import java.util.*;

public class P1_Yin_David_LifeModel extends GridModel<Boolean> implements GenerationListener{
    static Boolean[][] map;
    static GridModel<Boolean> model;
    static ArrayList<GenerationListener> a = new ArrayList<GenerationListener>();
    static int gen;
    int speed = 1;
    public P1_Yin_David_LifeModel(Boolean[][] grid) {
        super(grid);
        map = grid;
        model = new GridModel<Boolean>(grid);
        
        
        
    }

    
    
    public static void main(String[] args) {
        
        
    }
    public static void runLife(int gen) {
        print();
        for(int i = 0; i < gen;i++) {
            nextGeneration();
            System.out.println("stage: "+ (i+1));
            
        }
    }
    
    public static int rowCount(int row) {
        if(row < 0 || row >= model.getNumRows()) {
            return -1;
        }
        int counter = 0;
        for(int i = 0; i < model.getNumRows(); i++) {
            if(map[row][i] == true) {
                counter++;
            }
        }
        return counter;
    }
    
    public static int totalCount() {
        int counter = 0;
        for(int i = 0; i < model.getNumRows(); i++) {
            for(int j = 0; j < model.getNumCols(); j++) {
                if(map[i][j] == true) {
                    counter++;
                }
            }
        }
        return counter;
    }
    
    public static int colCount(int col) {
        if(col < 0 || col >= model.getNumCols()) {
            return -1;
        }
        int counter = 0;
        for(int i = 0; i < model.getNumCols(); i++) {
            if(map[i][col] == true) {
                counter++;
            }
        }
        
        return counter;
        
    }
    
    public static void nextGeneration() {
        Boolean[][] temp = new Boolean[model.getNumRows()][model.getNumCols()];
        
        for(int i = 0; i < model.getNumRows(); i ++) {
            for(int j = 0; j < model.getNumCols(); j++) {
                if( numAdjacentTruths(i,j) == 3) {
                    temp[i][j] = true;
                }else if(numAdjacentTruths(i,j) > 3) {
                    temp[i][j] = false;
                    
                }else if(numAdjacentTruths(i,j) < 2) {
                    temp[i][j] = false;
                }else if(model.getValueAt(i,j) == true && (numAdjacentTruths(i,j) == 2 || numAdjacentTruths(i,j) == 3)) {
                    temp[i][j] = true;
                }else {
                    temp[i][j] = false;
                }
            }
            
        }
        for(int i = 0; i < model.getNumRows(); i++) {
            for(int j = 0; j < model.getNumCols(); j++) {
                map[i][j] = temp[i][j];
                model.setValueAt(i, j, temp[i][j]);
            }
                
        }
    
    }
    
    
    public static int numAdjacentTruths(int row, int col) {
        int counter = 0;
        // Yoda says "Fall to the darkside, those who fail to respect boundaries will."
        for(int i = row-1; i <= row+1; i++) {
            for(int j = col-1; j <= col+1;j++) {
                if(isBounds(i,j)) {
                    if(model.getValueAt(i, j) == true) {
                        if(i != row || j != col) {
                            
                            counter++;
                        }
                    }
                }
            }
        }
        //System.out.println("row: " + row + "col: " + col+ " " + counter);
        return counter;
    }
    
    public static boolean isBounds(int i,int j) {
        if((i >=0 && i < model.getNumRows()) && (j >=0 && j < model.getNumCols())) {
            //System.out.println("true");
            
            return true;
        }
        return false;
    }

    public static Boolean[][] readFile() {
        Scanner in;
        try{
             in = new Scanner(new File("life100.txt"));
             map = new Boolean[in.nextInt()][in.nextInt()];
             
             
             while(in.hasNextInt()) {
                 model.setValueAt(in.nextInt(), in.nextInt(), true);
                 
             }
             
        }catch(IOException i){
             System.out.println("Error: " + i.getMessage());
        }
        
        
        return map;
    }
    
    public static void print() {
        int counter = 0;
        System.out.print("    ");
        for(int i = 0; i <= model.getNumRows(); i++) {
            if(counter < 10) {
                System.out.print(counter+" ");
                counter ++;
            }else {
                counter ++;
                counter = 0;
            }
            
            
        }
        //System.out.println("");
        System.out.println("");
        //int counter = 0;
        for(int i = 0; i < model.getNumRows(); i++) {
            if(i < 10) {
                System.out.print(" " + (i) + "  ");
            }else {
                System.out.print((i)+ "  ");
            }
            
            for(int j = 0; j < model.getNumCols(); j++) {
                //System.out.print();
                if(model.getValueAt(i, j) == true) {
                    System.out.print( "* ");
                    
                }else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
        
        
        
    }
    
    static void addGenerationListener(GenerationListener l) {
        a.add(l);
    }

    static void removeGenerationListener(GenerationListener l) {
        a.remove(l);
    }
    
    static void setGeneration(int g) {
        gen = g;
    }
    
    static int getGeneration() {
        return gen;
    }

    @Override
    public void generationChanged(int oldVal, int newVal) {
        // TODO Auto-generated method stub
        
    }
    
    int getSpeed() {
        return speed;
    }
    void setSpeed(int c) {
        speed = c;
        
    }
    
    void clearMap() {
        for(int i = 0; i < model.getNumRows(); i++) {
            for(int j = 0; j < model.getNumCols(); j++) {
                map[i][j] = false;
            }
        }
    }
    
    void setMap(Boolean[][] b) {
        for(int i = 0; i < model.getNumRows(); i++) {
            for(int j = 0; j < model.getNumCols(); j++) {
                map[i][j] = b[i][j];
            }
        }
        
    }
    
    
}
