import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class P1_Yin_David_LifeGUI_2 extends Application implements GenerationListener{
    BorderPane root;
    Stage stage;
    double SIZE = 600;
    Group r;
    Scene scene;
    Button clear;
    Button load;
    Slider slide;
    Button nextGen;
    P1_Yin_David_LifeModel model;
    Boolean[][] grid;
    Boolean[][] dGrid;
    BooleanGridPane view;
    Label lab;
    Button play;
    Button stop;
    Anime a;
    Slider speed;
    long elasped ;
    long lastUp;
    double delay = .1;
    
    int counter = 0;
    int speeder = 2;
    
    public static void main(String[] args) {
        launch();
    }
    @Override
    public void start(Stage s) throws Exception {
        
        stage = s;
        stage.setTitle("Grid Viewer");
        stage.setResizable(false);
        stage.sizeToScene();
        
        view = new BooleanGridPane();
        grid = new Boolean[10][10];
        
        root = new BorderPane();
        HBox box = new HBox();
        box.setPadding(new Insets(SIZE/100));
        root.setBottom(box);
        
        HBox b = new HBox();
        root.setTop(b);
        
        clear = new Button("Clear");
        box.getChildren().add(clear);
        MyMouseEventHandlerC m = new MyMouseEventHandlerC();
        clear.setOnMouseClicked(m);
        
        box.setSpacing(SIZE/10);
        
        MyMouseEventHandlerL l = new MyMouseEventHandlerL();

        
        slide = new Slider();
        slide.setMin(25);
        slide.setMax(50);
        slide.setShowTickMarks(true);
        slide.setShowTickLabels(true);
        box.getChildren().add(slide);
        Listener listen = new Listener();
        slide.valueProperty().addListener(listen);
        
        
        

        nextGen = new Button("Next Generation");
        box.getChildren().add(nextGen);
        MyMouseEventHandlerG g = new MyMouseEventHandlerG();
        nextGen.setOnMouseClicked(g);
        
        
        
        lab = new Label("Generation: "+ model.getGeneration());
        b.getChildren().add(lab);
        
        play = new Button("Play");
        MyMouseEventHandlerP p = new MyMouseEventHandlerP();
        play.setOnMouseClicked(p);
        b.getChildren().add(play);
        
        stop = new Button("Stop");
        MyMouseEventHandlerS st = new MyMouseEventHandlerS();
        stop.setOnMouseClicked(st);
        b.getChildren().add(stop);
        
        speed = new Slider();
        speed.setMin(.1);
        speed.setMax(.5);
        speed.setShowTickMarks(true);
        speed.setShowTickLabels(true);
        b.getChildren().add(speed);
        ListenS ls = new ListenS();
        speed.valueProperty().addListener(ls);
        
        
        
        
        
        
        
        r = new Group();    
        scene = new Scene(root, SIZE, SIZE);
        stage.setScene(scene);
        root.setCenter(r);
        
        
        for(int i  =  0; i < grid.length; i++) {
            for(int j = 0; j < grid[i].length; j++) {
                grid[i][j] = false;
                
            }
        }
        
        model = new P1_Yin_David_LifeModel(grid);
        
        view.setModel(model);
        view.setTileSize(25);
        
        
        
        
        r.getChildren().add(view);
        
        
        stage.show();
        
        MyClick myClick = new MyClick();
        r.addEventHandler(MouseEvent.MOUSE_CLICKED, myClick );
        
        

        
        
        
    }
    
    class MyMouseEventHandlerG implements EventHandler<MouseEvent>{

        @Override
        public void handle(MouseEvent e) {
            if(e.getEventType() == MouseEvent.MOUSE_CLICKED) {
                //model.setGeneration(model.getGeneration() + 1);
                lab.setText("Generation: " + model.getGeneration());
                model.nextGeneration();
                model.setGrid(grid);
                
                
            }
            
        }
        
    }
    
    public void update() {
        model.setGrid(grid);
        
        
        
        
    }
    class MyMouseEventHandlerS implements EventHandler<MouseEvent>{

        @Override
        public void handle(MouseEvent e) {
            if(e.getEventType() == MouseEvent.MOUSE_CLICKED) {
                a.stop();
            }
        }
    }
    
    class MyMouseEventHandlerP implements EventHandler<MouseEvent>{

        @Override
        public void handle(MouseEvent e) {
            if(e.getEventType() == MouseEvent.MOUSE_CLICKED) {
                
                a = new Anime();
                
                a.start();
                
                
            }
            
        }
        
    }
    
    class MyClick implements EventHandler<MouseEvent>{

        @Override
        public void handle(MouseEvent e) {
            if(e.getEventType() == MouseEvent.MOUSE_CLICKED) {
                int col = view.colForXPos(e.getX());
                int row = view.rowForYPos(e.getY());

                
                if(model.getValueAt(row, col) == false) {
                    view.cellChanged(row, col, true, false);
                    model.setValueAt(row,col, true);
                }else if(model.getValueAt(row, col) == true){
                    view.cellChanged(row, col, true, false);
                    model.setValueAt(row,col, false);
                    
                    
                    
                }
                Boolean b[][] = new Boolean[model.getNumRows()][model.getNumCols()];
                for(int i = 0; i < model.getNumRows(); i++) {
                    for(int j = 0; j < model.getNumCols(); j++) {
                        b[i][j] = model.getValueAt(i, j);
                    }
                }
                
                model.setMap(b);
                

                
            }
        }
    }
    
    
    class MyMouseEventHandlerC implements EventHandler<MouseEvent>{

        @Override
        public void handle(MouseEvent e) {
            if(e.getEventType() == MouseEvent.MOUSE_CLICKED) {
                
                Boolean[][] d = new Boolean[model.getNumRows()][model.getNumCols()];
                for(int i = 0; i < model.getNumRows(); i++) {
                    for(int j = 0; j < model.getNumCols(); j++) {
                        d[i][j] = false;
                    }
                } 
                model.clearMap();
                model.setGrid(d);
                model.setGeneration(0);
                lab.setText("Generation: " + model.getGeneration());
            
                
            }
            
    
        }
    }
    
    class Anime extends AnimationTimer{

        @Override
        public void handle(long arg) {
            elasped = (arg - lastUp);
            //model.setGeneration(model.getGeneration() + 1);
            //lab.setText("Generation: " + model.getGeneration());
        
            if((arg - lastUp) >= delay*1e9) {
                model.nextGeneration();
                update();
                lastUp = arg;
            
            }
            
            

            
            counter++;
            
            
                
            
            
            
        }
        
    }
    

    
    class MyMouseEventHandlerL implements EventHandler<MouseEvent>{

        @Override
        public void handle(MouseEvent e) {
            if(e.getEventType() == MouseEvent.MOUSE_CLICKED) {
                 FileChooser fileChooser = new FileChooser();
                 File selectedFile = fileChooser.showOpenDialog(null);
                 if(selectedFile == null) {
                     
                 }else {
                     
                 
                    Scanner in;
                    try{
                        in = new Scanner(selectedFile);
                        while(in.hasNext()) {
                            int r = in.nextInt();
                            int c = in.nextInt();
                            String str;
                            Boolean[][] n = new Boolean[r][c];
                            for(int i = 0; i < n.length; i++) {
                                for(int j = 0; j < n[i].length; j++) {
                                    str = in.next();
                                    if(str.equals("O")) {
                                        n[i][j] = true;
                                    }else {
                                        n[i][j] = false;
                                    }
                                }
                            }
                            model.setGrid(n);
                            
                            
                            
                        }
                         
                    }catch(IOException i){
                         System.out.println("Error: " + i.getMessage());
                    }
                 }
                
            }
        }
    }
    
    
    
    class Listener implements ChangeListener<Number>{

        @Override
        public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {

            view.setTileSize((double)arg2);

        }
        
    }
    
    class ListenS implements ChangeListener<Number>{

        @Override
        public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
            delay = (double)arg2;
            
        }
        
    }
    
    



    @Override
    public void generationChanged(int oldVal, int newVal) {
        model.setGeneration(model.getGeneration() + 1);
        lab.setText("Generation: " + model.getGeneration() +1);
        
    }
    
    

    
    
    
    
    
    
    
    

}